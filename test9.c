#include <stdio.h>
void swap(int*,int*);
void main()
{
  int a,b;
  printf("enter the number a:");
  scanf("%d",&a);
  printf("enter other number b :");
  scanf("%d",&b);
  swap(&a,&b);
  printf("after swapping a=%d and b=%d",a,b);
}
void swap(int* x,int* y)
{
  int temp=*x;
      *x=*y;
      *y=temp;
}
